# translation of knewstuff6.pot to Esperanto
# Esperantaj mesaĝoj por "knewstuff"
# Copyright (C) 1998,2002, 2003, 2004, 2005, 2007, 2008 Free Software Foundation, Inc.
# Wolfram Diestel <wolfram@steloj.de>, 1998.
# Heiko Evermann <heiko@evermann.de>, 2002, 2003.
# Matthias Peick <matthias@peick.de>, 2004, 2005.
# Oliver Kellogg <okellogg@users.sourceforge.net>,2007.
# Cindy McKee <cfmckee@gmail.com>, 2007, 2008.
# Axel Rousseau <axel@esperanto-jeunes.org>, 2009.
# Michael Moroni <michael.moroni@mailoo.org>, 2012.
#
# Minuskloj: ĉ ĝ ĵ ĥ ŝ ŭ   Majuskloj: Ĉ Ĝ Ĵ Ĥ Ŝ Ŭ
#
msgid ""
msgstr ""
"Project-Id-Version: knewstuff\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-05-17 02:04+0000\n"
"PO-Revision-Date: 2023-03-25 21:21+0100\n"
"Last-Translator: Oliver Kellogg <okellogg@users.sourceforge.net>\n"
"Language-Team: Esperanto <kde-i18n-eo@kde.org>\n"
"Language: eo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: translate-po (https://github.com/zcribe/translate-po)\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: attica/atticaprovider.cpp:168
#, kde-format
msgid "All categories are missing"
msgstr "Ĉiuj kategorioj mankas"

#: attica/atticaprovider.cpp:480
#, kde-format
msgctxt ""
"the price of a download item, parameter 1 is the currency, 2 is the price"
msgid ""
"This item costs %1 %2.\n"
"Do you want to buy it?"
msgstr ""
"Ĉi tiu ero kostas %1 %2.\n"
"Ĉu vi volas aĉeti ĝin?"

#: attica/atticaprovider.cpp:494
#, kde-format
msgid ""
"Your account balance is too low:\n"
"Your balance: %1\n"
"Price: %2"
msgstr ""
"La buĝeto de via kondo estas tro malalta:\n"
"Via buĝeto: %1\n"
"Prezo: %2"

#: attica/atticaprovider.cpp:537
#, kde-format
msgctxt "voting for an item (good/bad)"
msgid "Your vote was recorded."
msgstr "Via marko estis registrita."

#: attica/atticaprovider.cpp:552
#, kde-format
msgid "You are now a fan."
msgstr "Vi nun estas ŝatanto."

#: attica/atticaprovider.cpp:580 core/enginebase.cpp:199
#: opds/opdsprovider.cpp:287
#, kde-format
msgid ""
"The service is currently undergoing maintenance and is expected to be back "
"in %1."
msgstr "La servo nuntempe estas prizorgata kaj estas atendita reveni en %1."

#: attica/atticaprovider.cpp:585
#, kde-format
msgid "Network error %1: %2"
msgstr "Reta eraro %1: %2"

#: attica/atticaprovider.cpp:591
#, kde-format
msgid "Too many requests to server. Please try again in a few minutes."
msgstr "Tro multaj petoj al servilo. Bonvole reprovu post kelkaj minutoj."

#: attica/atticaprovider.cpp:594
#, kde-format
msgid ""
"The Open Collaboration Services instance %1 does not support the attempted "
"function."
msgstr ""
"La okazo %1 de Open Collaboration Services ne subtenas la provitan funkcion."

#: attica/atticaprovider.cpp:598
#, kde-format
msgid "Unknown Open Collaboration Service API error. (%1)"
msgstr "Nekonata eraro de API por Malferma Kunlabora Servo. (%1)"

#: core/commentsmodel.cpp:168
#, kde-format
msgctxt ""
"The value returned for an unknown role when requesting data from the model."
msgid "Unknown CommentsModel role"
msgstr "Nekonataj KomentojModela rolo"

#: core/engine.cpp:89
#, kde-format
msgid "Initializing"
msgstr "Pravalorizanta"

#: core/engine.cpp:99
#, kde-format
msgid "Loading provider information"
msgstr "Ŝarganta informojn pri provizanto"

#: core/engine.cpp:112
#, kde-format
msgid "Loading data from provider"
msgstr "Ŝarganta datumojn el provizanto"

#: core/engine.cpp:355
#, kde-format
msgid "Re: %1"
msgstr "Re: %1"

#: core/engine.cpp:388
#, kde-format
msgid "Installing"
msgstr "Instalanta"

#: core/engine.cpp:392
#, kde-format
msgid "Loading one preview"
msgid_plural "Loading %1 previews"
msgstr[0] "Ŝargi unu antaŭrigardon"
msgstr[1] "Ŝargi %1 antaŭrigardojn"

#: core/engine.cpp:396
#, kde-format
msgid "Loading data"
msgstr "Ŝarganta datumojn"

#: core/enginebase.cpp:43
#, kde-format
msgid ""
"An error occurred during the installation process:\n"
"%1"
msgstr ""
"Eraro okazis dum la instala procezo:\n"
"%1"

#: core/enginebase.cpp:79
#, kde-format
msgid "Configuration file does not exist: \"%1\""
msgstr "Agorda dosiero ne ekzistas: \"%1\""

#: core/enginebase.cpp:87
#, kde-format
msgid "Configuration file exists, but cannot be opened: \"%1\""
msgstr "Agorda dosiero ekzistas, sed ne povas esti malfermita: \"%1\""

#: core/enginebase.cpp:94
#, kde-format
msgid "Configuration file is invalid: \"%1\""
msgstr "Agorddosiero nevalidas: \"%1\""

#: core/enginebase.cpp:103
#, kde-format
msgid "Use"
msgstr "Uzi"

#: core/enginebase.cpp:121
#, kde-format
msgid ""
"Could not initialise the installation handler for %1:\n"
"%2\n"
"This is a critical error and should be reported to the application author"
msgstr ""
"Ne eblis praligi la instalilon por %1:\n"
"%2\n"
"Ĉi tio estas kritika eraro kaj devus esti raportita al la aŭtoro de la "
"aplikaĵo."

#: core/enginebase.cpp:274
#, kde-format
msgid "Loading of providers from file: %1 failed"
msgstr "Ŝargado de provizantoj el dosiero: %1 fiaskis"

#: core/enginebase.cpp:291
#, kde-format
msgid "Could not load get hot new stuff providers from file: %1"
msgstr "Ne eblis ŝargi akiri varmajn novajn provizantojn de dosiero: %1"

#: core/enginebase.cpp:322
#, kde-format
msgid "Error initializing provider."
msgstr "Eraro dum pravalorizado de provizanto."

#: core/installation.cpp:106
#, kde-format
msgid "Invalid item."
msgstr "Nevalida ero."

#: core/installation.cpp:113
#, kde-format
msgid "Download of item failed: no download URL for \"%1\"."
msgstr "Elŝutado de ero fiaskis. neniu elŝutligilo por \"%1\"."

#: core/installation.cpp:144
#, kde-format
msgid "Download of \"%1\" failed, error: %2"
msgstr "Elŝutado de \"%1\" fiaskis. Eraro: %2"

#: core/installation.cpp:153
#, kde-format
msgid ""
"Cannot install '%1' because it points to a web page. Click <a "
"href='%2'>here</a> to finish the installation."
msgstr ""
"Ne povas instali '%1' ĉar ĝi montras al retpaĝo. Alklaku <a href='%2'>ĉi "
"tie</a> por fini la instaladon."

#: core/installation.cpp:191
#, kde-format
msgid "Could not install \"%1\": file not found."
msgstr "Ne povis instali \"%1\": dosiero ne trovita."

#: core/installation.cpp:357 core/installation.cpp:600
#, kde-format
msgid "Installation of %1 failed: %2"
msgstr "Instalado de %1 malsukcesis: %2"

#: core/installation.cpp:391
#, kde-format
msgid "Could not determine the type of archive of the downloaded file %1"
msgstr "Ne eblis determini la tipon de arkivo de la elŝutita dosiero %1"

#: core/installation.cpp:403
#, kde-format
msgid "Failed to open the archive file %1. The reported error was: %2"
msgstr ""
"Malsukcesis malfermi la arkivan dosieron %1. La raportita eraro estis: %2"

#: core/installation.cpp:469
#, kde-format
msgid ""
"This file already exists on disk (possibly due to an earlier failed download "
"attempt). Continuing means overwriting it. Do you wish to overwrite the "
"existing file?"
msgstr ""
"Ĉi tiu dosiero jam ekzistas sur disko (eble pro pli frua malsukcesa elŝuta "
"provo). Daŭri signifas anstataŭi ĝin. Ĉu vi volas anstataŭigi la ekzistantan "
"dosieron?"

#: core/installation.cpp:472
#, kde-format
msgid "Overwrite File"
msgstr "Anstataŭigi dosieron"

#: core/installation.cpp:491
#, kde-format
msgid "Unable to move the file %1 to the intended destination %2"
msgstr "Ne eblas movi la dosieron %1 al la celita celo %2"

#: core/installation.cpp:514
#, kde-format
msgid ""
"The installation failed while attempting to run the command:\n"
"%1\n"
"\n"
"The returned output was:\n"
"%2"
msgstr ""
"La instalado malsukcesis dum provado ruli la komandon:\n"
"%1\n"
"\n"
"La resendita eligo estis:\n"
"%2"

#: core/installation.cpp:524
#, kde-format
msgid ""
"The installation failed with code %1 while attempting to run the command:\n"
"%2\n"
"\n"
"The returned output was:\n"
"%3"
msgstr ""
"La instalo malsukcesis kun la kodo %1 dum provado ruli la komandon:\n"
"%2\n"
"\n"
"La resendita eligo estis:\n"
"%3"

#: core/installation.cpp:565
#, kde-format
msgid ""
"The removal of %1 failed, as the installed file %2 could not be "
"automatically removed. You can attempt to manually delete this file, if you "
"believe this is an error."
msgstr ""
"La forigo de %1 malsukcesis, ĉar la instalita dosiero %2 ne povis esti "
"aŭtomate forigita. Vi povas provi mane forigi ĉi tiun dosieron, se vi "
"kredas, ke tio estas eraro."

#: core/installation.cpp:633
#, kde-format
msgid ""
"The uninstallation process failed to successfully run the command %1\n"
"The output of was: \n"
"%2\n"
"If you think this is incorrect, you can continue or cancel the "
"uninstallation process"
msgstr ""
"La malinstala procezo malsukcesis ruli la komandon %1\n"
"La eligo de estis: \n"
"%2\n"
"Se vi opinias, ke tio estas malĝusta, vi povas daŭrigi aŭ nuligi la "
"malinstalan procezon"

#: core/jobs/filecopyworker.cpp:49
#, kde-format
msgid "Could not open %1 for writing"
msgstr "Ne eblis malfermi %1 por skribado"

#: core/jobs/filecopyworker.cpp:52
#, kde-format
msgid "Could not open %1 for reading"
msgstr "Ne eblis malfermi %1 por legado"

#: core/transaction.cpp:137
#, kde-format
msgid ""
"Could not perform an installation of the entry %1 as it does not have any "
"downloadable items defined. Please contact the author so they can fix this."
msgstr ""
"Ne eblis fari instaladon de la eniro %1 ĉar ĝi ne havas iujn elŝuteblajn "
"erojn difinitajn. Bonvolu kontakti la aŭtoron por ke ili povu ripari ĉi tion."

#: core/transaction.cpp:246
#, kde-format
msgid "Pick Update Item"
msgstr "Elekti ĝisdatigaĵon"

#: core/transaction.cpp:248
#, kde-format
msgid ""
"Please pick the item from the list below which should be used to apply this "
"update. We were unable to identify which item to select, based on the "
"original item, which was named %1"
msgstr ""
"Bonvolu elekti la objekton el la suba listo, kiu devus esti uzata por apliki "
"ĉi tiun ĝisdatigon. Ni ne povis identigi kiun objekton elekti, surbaze de la "
"origina ero, kiu estis nomita %1"

#: core/transaction.cpp:267
#, kde-format
msgid ""
"We failed to identify a good link for updating %1, and are unable to perform "
"the update"
msgstr ""
"Ni malsukcesis identigi bonan ligilon por ĝisdatigi %1, kaj ne kapablas "
"plenumi la ĝisdatigon"

#: core/transaction.cpp:350
#, kde-format
msgid ""
"Failed to adopt '%1'\n"
"%2"
msgstr ""
"Malsukcesis adopti '%1'\n"
"%2"

#: qtquick/categoriesmodel.cpp:63
#, kde-format
msgctxt ""
"The first entry in the category selection list (also the default value)"
msgid "All Categories"
msgstr "Ĉiuj kategorioj"

#: qtquick/categoriesmodel.cpp:92
#, kde-format
msgctxt ""
"The string passed back in the case the requested category is not known"
msgid "Unknown Category"
msgstr "Nekonata Kategorio"

#. i18nd("knewstuff6", "Download New %1...").
#. *
#. @note For the sake of consistency, you should NOT override the text property, just set this one
#.
#: qtquick/qml/Button.qml:43
#, kde-format
msgctxt ""
"Used to construct the button's label (which will become Download New 'this "
"value'...)"
msgid "Stuff"
msgstr "Aĵoj"

#: qtquick/qml/Button.qml:44
#, kde-format
msgid "Download New %1..."
msgstr "Elŝuti novan %1..."

#: qtquick/qml/Dialog.qml:43
#, kde-format
msgctxt "The dialog title when we know which type of stuff is being requested"
msgid "Download New %1"
msgstr "Elŝuti Novan %1"

#: qtquick/qml/Dialog.qml:43
#, kde-format
msgctxt ""
"A placeholder title used in the dialog when there is no better title "
"available"
msgid "Download New Stuff"
msgstr "Elŝuti tre novajn aĵojn"

#: qtquick/qml/DownloadItemsSheet.qml:30
#, kde-format
msgid "Pick Your Installation Option"
msgstr "Elekti Vian Instal-Opcion"

#: qtquick/qml/DownloadItemsSheet.qml:40
#, kde-format
msgid ""
"Please select the option you wish to install from the list of downloadable "
"items below. If it is unclear which you should chose out of the available "
"options, please contact the author of this item and ask that they clarify "
"this through the naming of the items."
msgstr ""
"Bonvolu elekti la opcion, kiun vi volas instali el la listo de elŝuteblaj "
"aĵoj sube. Se ne klaras, kiun vi elektu el la disponeblaj opcioj, bonvolu "
"kontakti la aŭtoron de ĉi tiu aĵo kaj petu, ke ili klarigu tion per la "
"nomado de la eroj."

#: qtquick/qml/DownloadItemsSheet.qml:62
#, kde-format
msgid "Install"
msgstr "Instali"

#: qtquick/qml/EntryDetails.qml:49
#, kde-format
msgctxt ""
"A passive notification shown when installation of an item is initiated"
msgid "Installing %1 from %2"
msgstr "Instalante %1 de %2"

#: qtquick/qml/EntryDetails.qml:65
#, kde-format
msgctxt ""
"Status message to be shown when the entry is in the process of being "
"installed OR uninstalled"
msgid "Currently working on the item %1 by %2. Please wait..."
msgstr "Aktuale laboras pri la ero %1 de %2. Bonvolu atendi..."

#: qtquick/qml/EntryDetails.qml:67
#, kde-format
msgctxt ""
"Status message to be shown when the entry is in the process of being updated"
msgid "Currently updating the item %1 by %2. Please wait..."
msgstr "Aktuale ĝisdatiganta la eron %1 de %2. Bonvolu atendi..."

#: qtquick/qml/EntryDetails.qml:69
#, kde-format
msgctxt ""
"Status message which should only be shown when the entry has been given some "
"unknown or invalid status."
msgid ""
"This item is currently in an invalid or unknown state. <a href=\"https://"
"bugs.kde.org/enter_bug.cgi?product=frameworks-knewstuff\">Please report this "
"to the KDE Community in a bug report</a>."
msgstr ""
"Ĉi tiu aĵo estas nuntempe en nevalida aŭ nekonata stato. <a href=\"https://"
"bugs.kde.org/enter_bug.cgi?product=frameworks-knewstuff\">Bonvolu raporti "
"tion al la KDE-Komunumo en raporto pri eraro</a>."

#: qtquick/qml/EntryDetails.qml:83
#, kde-format
msgctxt ""
"Combined title for the entry details page made of the name of the entry, and "
"the author's name"
msgid "%1 by %2"
msgstr "%1 per %2"

#: qtquick/qml/EntryDetails.qml:86
#: qtquick/qml/private/entrygriddelegates/BigPreviewDelegate.qml:49
#: qtquick/qml/private/entrygriddelegates/ThumbDelegate.qml:42
#: qtquick/qml/private/entrygriddelegates/TileDelegate.qml:56
#, kde-format
msgctxt ""
"Request installation of this item, available when there is exactly one "
"downloadable item"
msgid "Install"
msgstr "Instali"

#: qtquick/qml/EntryDetails.qml:86
#: qtquick/qml/private/entrygriddelegates/BigPreviewDelegate.qml:49
#: qtquick/qml/private/entrygriddelegates/ThumbDelegate.qml:42
#: qtquick/qml/private/entrygriddelegates/TileDelegate.qml:56
#, kde-format
msgctxt ""
"Show installation options, where there is more than one downloadable item"
msgid "Install..."
msgstr "Instali..."

#: qtquick/qml/EntryDetails.qml:101 qtquick/qml/NewStuffItem.qml:29
#: qtquick/qml/private/entrygriddelegates/BigPreviewDelegate.qml:64
#: qtquick/qml/private/entrygriddelegates/ThumbDelegate.qml:57
#: qtquick/qml/private/entrygriddelegates/TileDelegate.qml:71
#, kde-format
msgctxt "Request updating of this item"
msgid "Update"
msgstr "Ĝisdatigi"

#: qtquick/qml/EntryDetails.qml:108 qtquick/qml/NewStuffItem.qml:36
#: qtquick/qml/Page.qml:154
#, kde-format
msgctxt "Request uninstallation of this item"
msgid "Uninstall"
msgstr "Malinstali"

#: qtquick/qml/EntryDetails.qml:151
#, kde-format
msgid "Comments and Reviews:"
msgstr "Komentoj kaj Recenzoj:"

#: qtquick/qml/EntryDetails.qml:153
#, kde-format
msgctxt ""
"A link which, when clicked, opens a new sub page with comments (comments "
"with or without ratings) for this entry"
msgid "%1 Reviews and Comments"
msgstr "%1 Recenzoj kaj Komentoj"

#: qtquick/qml/EntryDetails.qml:158
#, kde-format
msgid "Rating:"
msgstr "Pritakso:"

#: qtquick/qml/EntryDetails.qml:162
#, kde-format
msgid "Homepage:"
msgstr "Hejmpaĝo:"

#: qtquick/qml/EntryDetails.qml:163
#, kde-format
msgctxt ""
"A link which, when clicked, opens the website associated with the entry "
"(this could be either one specific to the project, the author's homepage, or "
"any other website they have chosen for the purpose)"
msgid "Open the homepage for %1"
msgstr "Malfermi la hejmpaĝon por %1"

#: qtquick/qml/EntryDetails.qml:168
#, kde-format
msgid "How To Donate:"
msgstr "Kiel Donaci:"

#: qtquick/qml/EntryDetails.qml:169
#, kde-format
msgctxt ""
"A link which, when clicked, opens a website with information on donation in "
"support of the entry"
msgid "Find out how to donate to this project"
msgstr "Ekscii kiel donaci al ĉi tiu projekto"

#: qtquick/qml/NewStuffItem.qml:22
#, kde-format
msgctxt "Request installation of this item"
msgid "Install"
msgstr "Instali"

#: qtquick/qml/NewStuffList.qml:68
#, kde-format
msgctxt "List option which will set the filter to show everything"
msgid "Show Everything"
msgstr "Montri Ĉion"

#: qtquick/qml/NewStuffList.qml:69
#, kde-format
msgctxt ""
"List option which will set the filter so only installed items are shown"
msgid "Installed Only"
msgstr "Nur instalita"

#: qtquick/qml/NewStuffList.qml:70
#, kde-format
msgctxt ""
"List option which will set the filter so only installed items with updates "
"available are shown"
msgid "Updateable Only"
msgstr "Ĝisdatigebla Nur"

#: qtquick/qml/NewStuffList.qml:82
#, kde-format
msgctxt ""
"List option which will set the sort order to based on when items were most "
"recently updated"
msgid "Show most recent first"
msgstr "Montri la plej freŝajn unue"

#: qtquick/qml/NewStuffList.qml:83
#, kde-format
msgctxt ""
"List option which will set the sort order to be alphabetical based on the "
"name"
msgid "Sort alphabetically"
msgstr "Ordigi alfabete"

#: qtquick/qml/NewStuffList.qml:84
#, kde-format
msgctxt "List option which will set the sort order to based on user ratings"
msgid "Show highest rated first"
msgstr "Montri plej alte taksitajn unue"

#: qtquick/qml/NewStuffList.qml:85
#, kde-format
msgctxt ""
"List option which will set the sort order to based on number of downloads"
msgid "Show most downloaded first"
msgstr "Montri plej elŝutitajn unue"

#: qtquick/qml/Page.qml:132
#, kde-format
msgctxt ""
"A message which is shown when the user attempts to display a specific entry "
"from a specific provider, but that entry isn't found"
msgid ""
"The entry you attempted to display, identified by the unique ID %1, could "
"not be found."
msgstr ""
"La enskribo, kiun vi provis montri, identigita per la unika ID %1, ne estis "
"trovita."

#: qtquick/qml/Page.qml:179
#, kde-format
msgid ""
"The content available here has been uploaded by users like you, and has not "
"been reviewed by your distributor for functionality or stability."
msgstr ""
"La enhavo disponebla ĉi tie estis alŝutita de uzantoj kiel vi, kaj ne estis "
"reviziita de via distribuisto por funkcieco aŭ stabileco."

#: qtquick/qml/Page.qml:213
#, kde-format
msgid "Tiles"
msgstr "Kaheloj"

#: qtquick/qml/Page.qml:215
#, kde-format
msgid "Icons"
msgstr "Piktogramoj"

#: qtquick/qml/Page.qml:217
#, kde-format
msgid "Preview"
msgstr "Antaŭrigardo"

#: qtquick/qml/Page.qml:232
#, kde-format
msgid "Detailed Tiles View Mode"
msgstr "Vidreĝimo de Detalaj Kaheloj"

#: qtquick/qml/Page.qml:240
#, kde-format
msgid "Icons Only View Mode"
msgstr "Nur-Piktograma Vidreĝimo"

#: qtquick/qml/Page.qml:248
#, kde-format
msgid "Large Preview View Mode"
msgstr "Grand-Antaŭrigarda Vidreĝimo"

#: qtquick/qml/Page.qml:258
#, kde-format
msgid "Everything"
msgstr "Ĉio"

#: qtquick/qml/Page.qml:260
#, kde-format
msgid "Installed"
msgstr "Instalitaj"

#: qtquick/qml/Page.qml:262
#, kde-format
msgid "Updateable"
msgstr "Ĝisdatigebla"

#: qtquick/qml/Page.qml:281
#, kde-format
msgctxt "List option which will set the filter to show everything"
msgid "Show All Entries"
msgstr "Montri ĉiujn enskribojn"

#: qtquick/qml/Page.qml:289
#, kde-format
msgctxt ""
"List option which will set the filter so only installed items are shown"
msgid "Show Only Installed Entries"
msgstr "Montri Nur Instalitajn Enskribojn"

#: qtquick/qml/Page.qml:297
#, kde-format
msgctxt ""
"List option which will set the filter so only installed items with updates "
"available are shown"
msgid "Show Only Updateable Entries"
msgstr "Montri Nur Ĝisdatigeblajn Enskribojn"

#: qtquick/qml/Page.qml:307
#, kde-format
msgid "Recent"
msgstr "Lastatempa"

#: qtquick/qml/Page.qml:309
#, kde-format
msgid "Alphabetical"
msgstr "Alfabeta"

#: qtquick/qml/Page.qml:311
#, kde-format
msgid "Rating"
msgstr "Pritakso"

#: qtquick/qml/Page.qml:313
#, kde-format
msgid "Downloads"
msgstr "Elŝutoj"

#: qtquick/qml/Page.qml:332
#, kde-format
msgctxt ""
"List option which will set the sort order to based on when items were most "
"recently updated"
msgid "Show Most Recent First"
msgstr "Montri Plej Lasttempajn Unue"

#: qtquick/qml/Page.qml:340
#, kde-format
msgctxt ""
"List option which will set the sort order to be alphabetical based on the "
"name"
msgid "Sort Alphabetically By Name"
msgstr "Ordigi Alfabete Laŭ Nomo"

#: qtquick/qml/Page.qml:348
#, kde-format
msgctxt "List option which will set the sort order to based on user ratings"
msgid "Show Highest Rated First"
msgstr "Montri Plej Altnivelajn Unue"

#: qtquick/qml/Page.qml:356
#, kde-format
msgctxt ""
"List option which will set the sort order to based on number of downloads"
msgid "Show Most Downloaded First"
msgstr "Montru Plej Elŝutitajn Unue"

#: qtquick/qml/Page.qml:365
#, kde-format
msgid "Upload..."
msgstr "Alŝuti..."

#: qtquick/qml/Page.qml:366
#, kde-format
msgid "Learn how to add your own hot new stuff to this list"
msgstr "Lernu kiel aldoni viajn proprajn novajn aĵojn al ĉi tiu listo"

#: qtquick/qml/Page.qml:374
#, kde-format
msgid "Go to..."
msgstr "Iri al..."

#: qtquick/qml/Page.qml:380 qtquick/qml/Page.qml:387
#, kde-format
msgid "Search..."
msgstr "Serĉi..."

#: qtquick/qml/Page.qml:424
#, kde-format
msgid "Category:"
msgstr "Kategorio:"

#: qtquick/qml/Page.qml:440
#, kde-format
msgid "Contribute your own…"
msgstr "Kontribui vian propran…"

#: qtquick/qml/Page.qml:506
#, kde-format
msgctxt ""
"A text shown beside a busy indicator suggesting that data is being fetched"
msgid "Loading more…"
msgstr "Ŝargante pli…"

#: qtquick/qml/private/EntryCommentDelegate.qml:110
#, kde-format
msgctxt ""
"Placeholder title for when a comment has no subject, but does have a rating"
msgid "<i>(no title)</i>"
msgstr "<i>(sen titolo)</i>"

#: qtquick/qml/private/EntryCommentDelegate.qml:142
#, kde-format
msgctxt ""
"The author label in case the comment was written by the author of the "
"content entry the comment is attached to"
msgid "%1 <i>(author)</i>"
msgstr "%1 <i>(aŭtoro)</i>"

#: qtquick/qml/private/EntryCommentsPage.qml:26
#, kde-format
msgctxt "Title for the page containing a view of the comments for the entry"
msgid "Comments and Reviews for %1"
msgstr "Komentoj kaj Recenzoj por %1"

#: qtquick/qml/private/EntryCommentsPage.qml:29
#, kde-format
msgctxt "Title for the item which is checked when all comments should be shown"
msgid "Show All Comments"
msgstr "Montri Ĉiujn Komentojn"

#: qtquick/qml/private/EntryCommentsPage.qml:35
#, kde-format
msgctxt ""
"Title for the item which is checked when only comments which are reviews "
"should be shown"
msgid "Show Reviews Only"
msgstr "Montri Nur Recenzojn"

#: qtquick/qml/private/EntryCommentsPage.qml:41
#, kde-format
msgctxt ""
"Title for the item which is checked when comments which are reviews, and "
"their children should be shown"
msgid "Show Reviews and Replies"
msgstr "Montri Recenzojn kaj Respondojn"

#: qtquick/qml/private/entrygriddelegates/BigPreviewDelegate.qml:140
#: qtquick/qml/private/entrygriddelegates/TileDelegate.qml:187
#, kde-format
msgctxt "The number of times the item has been downloaded"
msgid "%1 downloads"
msgstr "%1 elŝutoj"

#: qtquick/qml/private/entrygriddelegates/BigPreviewDelegate.qml:153
#: qtquick/qml/private/entrygriddelegates/TileDelegate.qml:166
#, kde-format
msgctxt ""
"Subheading for the tile view, located immediately underneath the name of the "
"item"
msgid "By <i>%1</i>"
msgstr "De <i>%1</i>"

#: qtquick/qml/private/entrygriddelegates/FeedbackOverlay.qml:44
#, kde-format
msgctxt ""
"Label for the busy indicator showing an item is being installed OR "
"uninstalled"
msgid "Working..."
msgstr "Laborante..."

#: qtquick/qml/private/entrygriddelegates/FeedbackOverlay.qml:46
#, kde-format
msgctxt ""
"Label for the busy indicator showing an item is in the process of being "
"updated"
msgid "Updating..."
msgstr "Ĝisdatiganta..."

#: qtquick/qml/private/entrygriddelegates/FeedbackOverlay.qml:48
#, kde-format
msgctxt ""
"Label for the busy indicator which should only be shown when the entry has "
"been given some unknown or invalid status."
msgid ""
"Invalid or unknown state. <a href=\"https://bugs.kde.org/enter_bug.cgi?"
"product=frameworks-knewstuff\">Please report this to the KDE Community in a "
"bug report</a>."
msgstr ""
"Nevalida aŭ nekonata ŝtato. <a href=\"https://bugs.kde.org/enter_bug.cgi?"
"product=frameworks-knewstuff\">Bonvolu raporti tion al la KDE-Komunumo en "
"raporto pri eraro</a>."

#: qtquick/qml/private/entrygriddelegates/FeedbackOverlay.qml:61
#, kde-format
msgctxt "Label for the busy indicator showing an item is installing"
msgid "Installing..."
msgstr "Instalanta..."

#: qtquick/qml/private/ErrorDisplayer.qml:13
#, kde-format
msgctxt "Title for a dialog box which shows error messages"
msgid "An Error Occurred"
msgstr "Eraro okazis"

#: qtquick/qml/private/ErrorDisplayer.qml:51
#, kde-format
msgid "Please try again later."
msgstr "Bonvolu reprovi poste."

#: qtquick/qml/private/GridTileDelegate.qml:113
#, kde-format
msgid "None"
msgstr "Neniu"

#: qtquick/qml/private/Rating.qml:80
#, kde-format
msgctxt ""
"A text representation of the rating, shown as a fraction of the max value"
msgid "(%1/%2)"
msgstr "(%1/%2)"

#: qtquick/qml/UploadPage.qml:51
#, kde-format
msgctxt "@knewstuff6"
msgid "Upload New Stuff: %1"
msgstr "Alŝuti Novajn Aĵojn: %1"

#: qtquick/qml/UploadPage.qml:83
#, kde-format
msgctxt ""
"A text shown beside a busy indicator suggesting that data is being fetched"
msgid "Updating information..."
msgstr "Ĝisdatigante informojn..."

#: qtquick/qml/UploadPage.qml:95
#, kde-format
msgctxt "The name of the KDE Store"
msgid "KDE Store"
msgstr "KDE-vendejo"

#: qtquick/qml/UploadPage.qml:101
#, kde-format
msgctxt "An unnamed provider"
msgid "Your Provider"
msgstr "Via Provizanto"

#: qtquick/qml/UploadPage.qml:109
#, kde-format
msgctxt ""
"Text for an action which causes the specified website to be opened using the "
"user's system default browser"
msgid "Open Website: %1"
msgstr "Malfermi Retejon: %1"

#: qtquick/qml/UploadPage.qml:114
#, kde-format
msgctxt ""
"Text for an action which will attempt to send an email using the user's "
"system default email client"
msgid "Send Email To: %1"
msgstr "Sendi Retpoŝton Al: %1"

#: qtquick/qml/UploadPage.qml:121
#, kde-format
msgctxt "A description of how to upload content to a generic provider"
msgid ""
"To upload new entries, or to add content to an existing entry on the KDE "
"Store, please open the website and log in. Once you have done this, you will "
"be able to find the My Products entry in the menu which pops up when you "
"click your user icon. Click on this entry to go to the product management "
"system, where you can work on your products ."
msgstr ""
"Por alŝuti novajn enskribojn, aŭ aldoni enhavon al ekzistanta enskribo en la "
"KDE-Butiko, bonvolu malfermi la retejon kaj ensaluti. Post kiam vi faris "
"tion, vi povos trovi la enskribon Miaj Produktoj en la menuo, kiu aperas "
"kiam vi alklaku vian uzantan ikonon. Alklaku ĉi tiun eniron por iri al la "
"produkta administra sistemo, kie vi povas labori pri viaj produktoj."

#: qtquick/qml/UploadPage.qml:122
#, kde-format
msgctxt "A description of how to upload content to the KDE Store specifically"
msgid ""
"To upload new entries, or to add content to an existing entry, please open "
"the provider's website and follow the instructions there. You will likely "
"need to create a user and log in to a product management system, where you "
"will need to follow the instructions for how to add. Alternatively, you "
"might be required to contact the managers of the site directly to get new "
"content added."
msgstr ""
"Por alŝuti novajn enskribojn, aŭ aldoni enhavon al ekzistanta enskribo, "
"bonvolu malfermi la retejon de la provizanto kaj sekvi la instrukciojn tie. "
"Vi verŝajne devos krei uzanton kaj ensaluti al produkta administra sistemo, "
"kie vi devos sekvi la instrukciojn pri kiel aldoni. Alternative, vi eble "
"devos kontakti la administrantojn de la retejo rekte por aldoni novan "
"enhavon."

#: qtquick/quickengine.cpp:132
#, kde-format
msgctxt ""
"An informational message which is shown to inform the user they are not "
"authorized to use GetHotNewStuff functionality"
msgid ""
"You are not authorized to Get Hot New Stuff. If you think this is in error, "
"please contact the person in charge of your permissions."
msgstr ""
"Vi ne estas rajtigita akiri Varmajn Novaĵojn. Se vi pensas, ke tio estas "
"erara, bonvolu kontakti la respondeculon pri viaj permesoj."

#: qtquick/searchpresetmodel.cpp:55
#, kde-format
msgctxt "Knewstuff5"
msgid "Back"
msgstr "Malantaŭen"

#: qtquick/searchpresetmodel.cpp:58
#, kde-format
msgctxt "Knewstuff5"
msgid "Popular"
msgstr "Populara"

#: qtquick/searchpresetmodel.cpp:61
#, kde-format
msgctxt "Knewstuff5"
msgid "Featured"
msgstr "Prezentita"

#: qtquick/searchpresetmodel.cpp:64
#, kde-format
msgctxt "Knewstuff5"
msgid "Restart"
msgstr "Restarigi"

#: qtquick/searchpresetmodel.cpp:67
#, kde-format
msgctxt "Knewstuff5"
msgid "New"
msgstr "Nova"

#: qtquick/searchpresetmodel.cpp:70
#, kde-format
msgctxt "Knewstuff5"
msgid "Home"
msgstr "Hejmen"

#: qtquick/searchpresetmodel.cpp:73
#, kde-format
msgctxt "Knewstuff5"
msgid "Shelf"
msgstr "Breto"

#: qtquick/searchpresetmodel.cpp:76
#, kde-format
msgctxt "Knewstuff5"
msgid "Up"
msgstr "Supren"

#: qtquick/searchpresetmodel.cpp:79
#, kde-format
msgctxt "Knewstuff5"
msgid "Recommended"
msgstr "Rekomendita"

#: qtquick/searchpresetmodel.cpp:82
#, kde-format
msgctxt "Knewstuff5"
msgid "Subscriptions"
msgstr "Abonoj"

#: qtquick/searchpresetmodel.cpp:85
#, kde-format
msgctxt "Knewstuff5"
msgid "All Entries"
msgstr "Ĉiuj Enskriboj"

#: qtquick/searchpresetmodel.cpp:88
#, kde-format
msgctxt "Knewstuff5"
msgid "Search Preset: %1"
msgstr "Serĉo Antaŭagordita: %1"

#: tools/knewstuff-dialog/main.cpp:31
#, kde-format
msgid ""
"The KNSRC file you want to show. If none is passed, you will be presented "
"with a dialog which lets you switch between all the config files installed "
"into the systemwide knsrc file location"
msgstr ""
"La KNSRC-dosieron, kiun vi volas montri. Se neniu estas preterpasita, vi "
"estos prezentita kun dialogo, kiu ebligas al vi ŝanĝi inter ĉiuj "
"agordosieroj instalitaj en la tutsistema knsrc-dosierloko."

#: tools/knewstuff-dialog/main.cpp:35
#, kde-format
msgid ""
"A kns url to show information from. The format for a kns url is kns://"
"knsrcfile/providerid/entryid\n"
"'knsrcfile'\n"
"is the name of a knsrc file as might be passed directly through this tool's "
"knsrcfile argument\n"
"'providerid'\n"
"is the hostname of the provider the entry should exist on\n"
"'entryid'\n"
"is the unique ID of an entry found in the provider specified by the knsrc "
"file.\n"
" An example of such a url is kns://peruse.knsrc/api.kde-look.org/1316714"
msgstr ""
"Kns-url por montri informojn de. La formato por kns-url estas kns://"
"knsrcfile/providerid/entryid\n"
"'knsrcfile'\n"
"is la nomo de knsrc-dosiero kiel eble pasigi rekte tra la knsrcfile "
"argumento de ĉi tiu ilo\n"
"'providerid'\n"
"is la gastiga nomo de la provizanto la eniro devus ekzisti sur\n"
"'entryid'\n"
"is la unika ID de eniro trovita en la provizanto specifita de la knsrc-"
"dosiero.\n"
" Ekzemplo de tia url estas kns://peruse.knsrc/api.kde-look.org/1316714"

#: widgets/action.cpp:28 widgets/button.cpp:70
#, kde-format
msgid "Download New Stuff..."
msgstr "Elŝuti tre novajn aĵojn..."
